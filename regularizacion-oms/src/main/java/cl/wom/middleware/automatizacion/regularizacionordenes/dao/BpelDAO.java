package cl.wom.middleware.automatizacion.regularizacionordenes.dao;

import org.apache.log4j.Logger;

/**
 * DAO que ejecuta operaciones en la base de datos Bpel.
 *
 * @author Eduardo Mascayano - Tinet
 */
public class BpelDAO extends BaseDAO {

	private final Logger log = Logger.getLogger(BpelDAO.class);

	/**
	 * Constructor.
	 * 
	 * @param url
	 *            url.
	 * @param user
	 *            user.
	 * @param password
	 *            password.
	 */
	public BpelDAO(String url, String user, String password) {

		this.url = url;
		this.user = user;
		this.password = password;
	}
}
