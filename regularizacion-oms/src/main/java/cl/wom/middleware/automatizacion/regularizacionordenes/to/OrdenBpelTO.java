package cl.wom.middleware.automatizacion.regularizacionordenes.to;

import java.io.Serializable;

public class OrdenBpelTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String omId;

	public String getOmId() {
		return omId;
	}

	public void setOmId(String omId) {
		this.omId = omId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrdenBpelTO [omId=");
		builder.append(omId);
		builder.append("]");
		return builder.toString();
	}

}
