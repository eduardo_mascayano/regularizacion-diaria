package cl.wom.middleware.automatizacion.actualizacionovedades.exception;

/**
 * Excepción de Negocio.
 *
 * @author Eduardo Mascayano - Tinet
 * @date 01/2019
 */
public class AplicacionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AplicacionException(String mensaje, Exception e) {

        super(mensaje, e);
    }

    public AplicacionException(String mensaje) {

        super(mensaje);
    }
}
