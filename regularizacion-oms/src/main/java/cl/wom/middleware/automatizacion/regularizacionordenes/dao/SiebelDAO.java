package cl.wom.middleware.automatizacion.regularizacionordenes.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import cl.wom.middleware.automatizacion.regularizacionordenes.exception.AplicacionException;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenBscsTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSiebelTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSincroTO;
import oracle.jdbc.OracleTypes;

/**
 * DAO que ejecuta operaciones en la base de datos Siebel.
 *
 * @author Eduardo Mascayano - Tinet
 */
public class SiebelDAO extends BaseDAO {

	private final Logger log = Logger.getLogger(SiebelDAO.class);

	/**
	 * Constructor.
	 * 
	 * @param url
	 *            url.
	 * @param user
	 *            user.
	 * @param password
	 *            password.
	 */
	public SiebelDAO(String url, String user, String password) {

		this.url = url;
		this.user = user;
		this.password = password;
	}

	public List<OrdenSiebelTO> regularizarPortabilidadesEnTransito() {

		log.info("[regularizarPortabilidadesEnTransito] Inicio");
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		List<OrdenSiebelTO> ordenes = new ArrayList<>();
		OrdenSiebelTO orden = null;

		try {
			conn = getConnection();
			if (conn != null) {
				String query = "{ ? = call FN_REGULARIZA_PORT_TRANSITO}";
				log.info("[regularizarPortabilidadesEnTransito] query: " + query);
				conn.setAutoCommit(false);
				cstmt = conn.prepareCall(query);

				cstmt.registerOutParameter(1, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					orden = new OrdenSiebelTO();
					orden.setRowId(getString(rs, "rowId"));
					orden.setContractId(getLong(rs, "contractId"));
					orden.setStatusOrdenRegularizada(getString(rs, "statusRegularizada"));
					ordenes.add(orden);
				}

			}
		} catch (SQLException e) {
			throw new AplicacionException("Error al regularizar ordenes en tránsito en Siebel", e);
		} catch (Exception e) {
			throw new AplicacionException("Error general al regularizar ordenes en tránsito en Siebel", e);
		} finally {
			closeCSConnection(cstmt, conn);
		}
		log.info("[regularizarPortabilidadesEnTransito] Fin");
		return ordenes;
	}
	
	
	public List<OrdenSiebelTO> regularizarPortabilidadesCanceladas() {

		log.info("[regularizarPortabilidadesCanceladas] Inicio");
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		List<OrdenSiebelTO> ordenes = new ArrayList<>();
		OrdenSiebelTO orden = null;

		try {
			conn = getConnection();
			if (conn != null) {
				String query = "{ ? = call FN_REGULARIZA_PORT_CANCEL}";
				log.info("[regularizarPortabilidadesCanceladas] query: " + query);
				conn.setAutoCommit(false);
				cstmt = conn.prepareCall(query);

				cstmt.registerOutParameter(1, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					orden = new OrdenSiebelTO();
					orden.setContractId(getLong(rs, "contractId"));
					orden.setStatusOrdenRegularizada(getString(rs, "statusRegularizada"));
					ordenes.add(orden);
				}

			}
		} catch (SQLException e) {
			throw new AplicacionException("Error al regularizar portabilidades canceladas en Siebel", e);
		} catch (Exception e) {
			throw new AplicacionException("Error general al regularizar portabilidades canceladas en Siebel", e);
		} finally {
			closeCSConnection(cstmt, conn);
		}
		log.info("[regularizarPortabilidadesCanceladas] Fin");
		return ordenes;
	}
	
	public boolean confirmaTelefonoEnSiebel(OrdenSincroTO sincro) {

		log.info("[confirmaTelefonoEnSiebel] Inicio");
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		boolean resultado = false;

		try {
			conn = getConnection();
			if (conn != null) {
				String query = "{ ? = call FN_FILTRA_TELEFONOS_CONTRATOS (?)}";
				log.info("[confirmaTelefonoEnSiebel] query: " + query);
				conn.setAutoCommit(false);
				cstmt = conn.prepareCall(query);

				cstmt.registerOutParameter(1, OracleTypes.CURSOR);
				cstmt.setLong(2, sincro.getOrdenSiebel().getContractId());
				cstmt.execute();

				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					resultado = true;
				}
			}
		} catch (SQLException e) {
			throw new AplicacionException("Error al confirmar número de teléfono del contrato en Siebel", e);
		} catch (Exception e) {
			throw new AplicacionException("Error general al confirmar número de teléfono del contrato en Siebel", e);
		} finally {
			closeCSConnection(cstmt, conn);
		}
		log.info("[confirmaTelefonoEnSiebel] Fin");
		return resultado;
	}
}
