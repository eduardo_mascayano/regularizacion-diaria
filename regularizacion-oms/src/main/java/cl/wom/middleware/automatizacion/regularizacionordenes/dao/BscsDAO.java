package cl.wom.middleware.automatizacion.regularizacionordenes.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import cl.wom.middleware.automatizacion.regularizacionordenes.exception.AplicacionException;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenBscsTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSiebelTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSincroTO;
import oracle.jdbc.OracleTypes;

/**
 * DAO que ejecuta operaciones en la base de datos Bscs.
 *
 * @author Eduardo Mascayano - Tinet
 */
public class BscsDAO extends BaseDAO {

	private final Logger log = Logger.getLogger(BscsDAO.class);

	/**
	 * Constructor.
	 * 
	 * @param url
	 *            url.
	 * @param user
	 *            user.
	 * @param password
	 *            password.
	 */
	public BscsDAO(String url, String user, String password) {

		this.url = url;
		this.user = user;
		this.password = password;
	}
	
	public OrdenSincroTO consultarNumTelefonoContratos(OrdenSiebelTO ordenSiebel) {

		log.info("[consultarNumTelefonoContratos] Inicio");
		CallableStatement cstmt = null;
		ResultSet rs = null;
		Connection conn = null;
		OrdenSincroTO ordenSincro = null;

		try {
			conn = getConnection();
			if (conn != null) {
				String query = "{ ? = call FN_CONSULTA_TELEFONOS_CONTRATOS (?)}";
				log.info("[consultarNumTelefonoContratos] query: " + query);
				conn.setAutoCommit(false);
				cstmt = conn.prepareCall(query);

				cstmt.registerOutParameter(1, OracleTypes.CURSOR);
				cstmt.setLong(2, ordenSiebel.getContractId());
				cstmt.execute();

				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					ordenSincro = new OrdenSincroTO();
					OrdenBscsTO ordenBscs = new OrdenBscsTO();
					ordenSincro.setOrdenSiebel(ordenSiebel);
					ordenBscs.setNumTelefono(getLong(rs, "numTelefono"));
					ordenSincro.setOrdenBscs(ordenBscs);
				}

			}
		} catch (SQLException e) {
			throw new AplicacionException("Error al consultar los número de teléfono de contratos en Bscs", e);
		} catch (Exception e) {
			throw new AplicacionException("Error al consultar los número de teléfono de contratos en Bscs", e);
		} finally {
			closeCSConnection(cstmt, conn);
		}
		log.info("[consultarNumTelefonoContratos] Fin");
		return ordenSincro;
	}
	
}
