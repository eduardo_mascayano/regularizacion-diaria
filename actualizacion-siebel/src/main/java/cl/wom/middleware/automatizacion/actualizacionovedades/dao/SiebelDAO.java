package cl.wom.middleware.automatizacion.actualizacionovedades.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import cl.wom.middleware.automatizacion.actualizacionovedades.exception.AplicacionException;

/**
 * DAO que ejecuta operaciones en la base de datos Siebel.
 *
 * @author Eduardo Mascayano - Tinet
 */
public class SiebelDAO extends BaseDAO {

    private final Logger log = Logger.getLogger(SiebelDAO.class);

    /**
     * Constructor.
     * 
     * @param url url.
     * @param user user.
     * @param password password.
     */
    public SiebelDAO(String url, String user, String password) {

        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void actualizarNovedadesSiebel() {

        log.info("[actualizarNovedadesSiebel] Inicio");
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = getConnection();
            if (conn != null) {
                // String query = "{call SIEBEL.PR_ACTUALIZA_NOVEDADES}";
                String query = "{call PR_ACTUALIZA_NOVEDADES}";
                log.info("[actualizarNovedadesSiebel] query: " + query);
                conn.setAutoCommit(false);
                cstmt = conn.prepareCall(query);
                cstmt.executeUpdate();
                //cstmt.executeQuery();
            }
        } catch (SQLException e) {
            throw new AplicacionException("Error al actualizar novedades en Siebel", e);
        } catch (Exception e) {
            throw new AplicacionException("Error general al actualizar novedades en Siebel", e);
        } finally {
            closeCSConnection(cstmt, conn);
        }
        log.info("[actualizarNovedadesSiebel] Fin");
    }
}
