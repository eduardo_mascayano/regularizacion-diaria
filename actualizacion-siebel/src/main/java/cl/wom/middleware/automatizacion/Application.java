package cl.wom.middleware.automatizacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Clase principal Spring Boot. Proyecto: 
 * MA-ITD-19002 Regularización Diaria > Actualización Novedades en Siebel
 *
 * @author Eduardo Mascayano - Tinet
 * @date 01/2019
 */
@SpringBootApplication
@ComponentScan
@EnableScheduling
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

}
