package cl.wom.middleware.automatizacion.regularizacionordenes.proceso;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.regularizacionordenes.impl.ImplementadorRegularizacionOrdenes;
import cl.wom.middleware.automatizacion.regularizacionordenes.mail.SimpleEmailController;
import cl.wom.middleware.automatizacion.regularizacionordenes.util.EmailUtil;

/**
 * Proceso que controla la ejecución de las regularización de órdenes.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class ProcesoRegularizaOrdenes {

	private final Logger log = Logger.getLogger(ProcesoRegularizaOrdenes.class);

	@Autowired
	private Environment env;

	@Autowired
	private ImplementadorRegularizacionOrdenes regularizador;

	@Autowired
	SimpleEmailController envioEmail;

	@Autowired
	private EmailUtil emailUtil;

	public void init() {

		log.info("[init] Inicio");

		try {
			regularizador.regularizarOrdenes();
			envioEmail.sendEmail(emailUtil.generarEmailExito());
		} catch (Exception e) {
			log.error("[init] Error al ejecutar proceso de Regularización de OMs", e);
			envioEmail.sendEmail(emailUtil.generarEmailError(e.getMessage()));
		}
		log.info("[init] Fin");
	}
}
