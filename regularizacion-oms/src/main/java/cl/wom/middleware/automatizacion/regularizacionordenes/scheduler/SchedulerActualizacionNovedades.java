package cl.wom.middleware.automatizacion.regularizacionordenes.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.regularizacionordenes.proceso.ProcesoRegularizaOrdenes;

/**
 * Scheduler para iniciar proceso de regularización de Om's.
 *
 * @author Eduardo Mascayano - Tinet
 */

@Component
public class SchedulerActualizacionNovedades {

	private final Logger log = Logger.getLogger(SchedulerActualizacionNovedades.class);

	@Autowired
	ProcesoRegularizaOrdenes proceso;

	/**
	 * Ejecuta el proceso de regularización de Lunes a Domingo (Configurable).
	 * 
	 */
	// @Scheduled(cron = "${horario.ejecucion.scheduler}", zone="America/Santiago")
	@Scheduled(fixedDelayString = "3000")
	public void iniciarProcesoRegularizacionOms() {

		log.info("[iniciarProcesoRegularizacionOms] Inicio");
		proceso.init();
		log.info("[iniciarProcesoRegularizacionOms] Fin");

	}
}
