package cl.wom.middleware.automatizacion.actualizacionovedades.mail;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:${APP_ENV}")
public class SimpleEmailController {

    private final Logger log = Logger.getLogger(SimpleEmailController.class);

    @Autowired
    private JavaMailSenderImpl sender;

    @Autowired
    private Environment env;

    public void sendEmail(String html) {

        try {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            Properties props = sender.getJavaMailProperties();
            props.put(env.getProperty("email.server.ssl"), env.getProperty("spring.mail.host"));
            sender.setJavaMailProperties(props);

            helper.setTo(env.getProperty("email.destino"));
            helper.setText(html, true);
            helper.setSubject(env.getProperty("email.subject"));
            log.info("[sendEmail] Antes de enviar email resultado proceso de Actualización Novedades en Siebel");
            sender.send(message);

        } catch (Exception e) {
            log.error(
                    "[sendEmail] Error al enviar email informe con resultado del proceso de Actualización Novedades en Siebel",
                    e);
        }
    }
}