package cl.wom.middleware.automatizacion.actualizacionovedades.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.actualizacionovedades.dao.SiebelDAO;
import cl.wom.middleware.automatizacion.actualizacionovedades.exception.AplicacionException;

/**
 * Clase utilitaria para conexion a FTP.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class FTPUtil {

    private final Logger log = Logger.getLogger(FTPUtil.class);

    @Autowired
    private Environment env;

    /**
     * 
     * Metodo obtenerArchivoDesdeFTP.
     *
     * @param solicitudId id de la solicitud.
     * @param path path.
     * @param propertyRepository repositorio de propiedades.
     * @return archivo en bytes.
     */
    public InputStream obtenerArchivoDesdeFTP() {

        log.info("[obtenerArchivoDesdeFTP] Inicio");
        String servidor = env.getProperty("servidor.ftp");
        String usuario = env.getProperty("usuario.ftp");
        String password = env.getProperty("password.ftp");
        String pathBase = env.getProperty("ruta.base.ftp");
        String file = env.getProperty("nombre.archivo");
        String directorioFinal = pathBase + "/";

        FTPClient cliente = new FTPClient();
        InputStream archivo = null;
        try {
            cliente.connect(servidor);
            boolean estaConectado = cliente.login(usuario, password);
            log.info("[obtenerArchivoDesdeFTP] Conexion exitosa? " + estaConectado);
            if (estaConectado) {
                cliente.enterLocalPassiveMode();
                cliente.changeWorkingDirectory(directorioFinal);
                cliente.setFileType(FTP.BINARY_FILE_TYPE);
                archivo = cliente.retrieveFileStream(file);
                cliente.completePendingCommand();
                cliente.logout();
                cliente.disconnect();
            }
        } catch (SocketException e) {
            throw new AplicacionException("Error conexión FTP para obtener archivo carga novedades.", e);
        } catch (IOException e) {
            throw new AplicacionException("Error de lectura archivo carga novedades en FTP.", e);
        } finally {
            if (cliente.isConnected()) {
                try {
                    cliente.logout();
                    cliente.disconnect();
                } catch (IOException e) {
                    log.error("[obtenerArchivoDesdeFTP][IOException] Error al desconectarse de FTP: ", e);
                }
            }
        }
        log.info("[obtenerArchivoDesdeFTP] Fin");
        return archivo;
    }

}
