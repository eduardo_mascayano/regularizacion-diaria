package cl.wom.middleware.automatizacion.actualizacionovedades.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.actualizacionovedades.exception.AplicacionException;

/**
 * Utilitario para manejo de archivos.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class ArchivoUtil {

    private final Logger log = Logger.getLogger(ArchivoUtil.class);

    @Autowired
    private Environment env;

    public void validarArchivo() {

        try (BufferedReader br = new BufferedReader(new FileReader(env.getProperty("ruta.directory.object")))) {

            String linea;
            if ((linea = br.readLine()) == null) {
                log.info("Archivo sin registros");
                throw new AplicacionException("Archivo carga novedades no contiene registros");
            }

        } catch (FileNotFoundException e) {
            throw new AplicacionException("Archivo carga novedades no existe", e);
        } catch (IOException e) {
            throw new AplicacionException("Error al intentar leer el archivo de carga novedades", e);
        }
    }

    public void copiarArchivoAOracleDirectoryObject(InputStream archivoIn) {

        File targetFile = new File(env.getProperty("ruta.directory.object"));
        try {
            FileUtils.copyInputStreamToFile(archivoIn, targetFile);
        } catch (IOException e) {
            throw new AplicacionException("Error al copiar archivo de carga " + env.getProperty("nombre.archivo")
                    + " a directory object de Oracle", e);
        }
    }

}
