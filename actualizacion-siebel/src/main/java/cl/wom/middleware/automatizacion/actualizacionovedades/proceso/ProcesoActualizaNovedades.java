package cl.wom.middleware.automatizacion.actualizacionovedades.proceso;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.actualizacionovedades.dao.SiebelDAO;
import cl.wom.middleware.automatizacion.actualizacionovedades.mail.SimpleEmailController;
import cl.wom.middleware.automatizacion.actualizacionovedades.util.ArchivoUtil;
import cl.wom.middleware.automatizacion.actualizacionovedades.util.EmailUtil;
import cl.wom.middleware.automatizacion.actualizacionovedades.util.FTPUtil;

/**
 * Proceso que valida y ejecuta la carga y actualización de las novedades en
 * Siebel.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class ProcesoActualizaNovedades {

    private final Logger log = Logger.getLogger(ProcesoActualizaNovedades.class);

    @Autowired
    private Environment env;

    @Autowired
    private ArchivoUtil archivoUtil;

    @Autowired
    private FTPUtil ftpUtil;

    @Autowired
    SimpleEmailController envioEmail;

    @Autowired
    private EmailUtil emailUtil;

    public void init() {

        log.info("[init] Inicio");

        try {
            SiebelDAO dao = new SiebelDAO(env.getProperty("siebel.datasource.url"),
                    env.getProperty("siebel.datasource.username"), env.getProperty("siebel.datasource.password"));

            InputStream archivo = ftpUtil.obtenerArchivoDesdeFTP();
            archivoUtil.copiarArchivoAOracleDirectoryObject(archivo);
            archivoUtil.validarArchivo();
            dao.actualizarNovedadesSiebel();
            envioEmail.sendEmail(emailUtil.generarEmailExito());
        } catch (Exception e) {
            log.error("[init] Error al ejecutar proceso de Actualización Novedades en Siebel", e);
            envioEmail.sendEmail(emailUtil.generarEmailError(e.getMessage()));
        }
        log.info("[init] Fin");
    }

}
