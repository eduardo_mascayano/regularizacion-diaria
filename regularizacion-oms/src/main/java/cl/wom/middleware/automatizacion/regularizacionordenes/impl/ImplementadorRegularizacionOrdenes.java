package cl.wom.middleware.automatizacion.regularizacionordenes.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.regularizacionordenes.dao.BpelDAO;
import cl.wom.middleware.automatizacion.regularizacionordenes.dao.BscsDAO;
import cl.wom.middleware.automatizacion.regularizacionordenes.dao.SiebelDAO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSiebelTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.to.OrdenSincroTO;
import cl.wom.middleware.automatizacion.regularizacionordenes.util.ArchivoUtil;

/**
 * Ejecuta las regularizaciones de las órdenes.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class ImplementadorRegularizacionOrdenes {

	private final Logger log = Logger.getLogger(ImplementadorRegularizacionOrdenes.class);

	@Autowired
	private Environment env;
	
	@Autowired
	private ArchivoUtil archivoUtil;
	
	SiebelDAO siebelDao;
	BpelDAO bpelDao;
	BscsDAO bscsDao;

	public void regularizarOrdenes() {
		
		siebelDao = new SiebelDAO(env.getProperty("siebel.datasource.url"),
				env.getProperty("siebel.datasource.username"), env.getProperty("siebel.datasource.password"));

		bpelDao = new BpelDAO(env.getProperty("bpel.datasource.url"),
				env.getProperty("bpel.datasource.username"), env.getProperty("bpel.datasource.password"));

		bscsDao = new BscsDAO(env.getProperty("bscs.datasource.url"),
				env.getProperty("bscs.datasource.username"), env.getProperty("bscs.datasource.password"));
		
		/**
		 * TODO: 
		 * - Hacer mecanismo para identificar qué regularización falló y alertar mediante email a un operador:
		 * - Cada regularización debe ser atómica en misma. Ej: Si falla la no. 3, no debe impedir ejecutar la no. 4
		 */
		
		List<OrdenSiebelTO> portEnTransito = regularizarPortabilidadesEnTransito();

		List<OrdenSiebelTO> portCanceladas = regularizarPortabilidadesCanceladas();

		sincronizarContratosPortabilidadesCanceladas(portEnTransito, portCanceladas);

		regularizarTimeOutFlujoCreateCustomer();

		regularizarOmsNoLlegadasABpel();

		regularizarOmsDeTransferenciaPIVCRM();

		regularizarOmsDeModificacionPIVCRM();

		regularizarOmsDeTransferenciaGeneral();

		regularizarOmsDeModificacionGeneral();

		regularizarOmsDeVentaGeneral();
		
		regularizarOmsDeResumirGeneral();

		regularizarOmsDeSuspenderGeneral();

		regularizarOmsDeDesconGeneral();

		regularizarTransfDesconEstadoCancelar();

		regularizarTransfDesconEstadoCompletar();

		regularizarTransfDesconEstadoError();

		regularizarTransfDesconEstadoPIVOpen();

		regularizarOmsSinLineas();

		regularizarRevisionFinal();

	}

	/** 1.1    PORTABILIDADES EN TRANSITO */
	public List<OrdenSiebelTO> regularizarPortabilidadesEnTransito() {		
		/**
		 * TODO
		 * + Invocar SP en SIEBEL (SELECT/UPDATE)
		 * + Devolver listado de: CO_ID | status actualizado
		 * */
		return siebelDao.regularizarPortabilidadesEnTransito();
	}

	
	/** 1.2    PORTABILIDADES CANCELADAS */
	public List<OrdenSiebelTO> regularizarPortabilidadesCanceladas() {
		/**
		 * TODO
		 * + Invocar SP en SIEBEL (SELECT/UPDATE)
		 * + Devolver listado de: CO_ID | status actualizado
		 * */
		return siebelDao.regularizarPortabilidadesCanceladas();
	}

	/** 1.3    SINCRONIZACIÓN DE CONTRATOS DE PORTABILIDADES CANCELADAS */
	public void sincronizarContratosPortabilidadesCanceladas(List<OrdenSiebelTO> portEnTransito,
			List<OrdenSiebelTO> portCanceladas) {
		
		/**
		 * TODO
		 * - LOOP: 
		 * 		+ Invocar SP en BSCS (SELECT)
		 * 		+ Devolver registro con número de teléfono del contrato
		 * 		+ Invocar SP en SIEBEL (SELECT)
		 * 		+ Devolver registro de CO_ID
		 * 		+ Armar Listado de CO_ID devueltos por la query
		 * + Generar CSV con listado de: row_id | CO_ID | status actualizado
		 * */
		
		List<OrdenSincroTO> ordenesSincro = new ArrayList<>();
		OrdenSincroTO ordenSincro = null;

		for (OrdenSiebelTO ordenTransito : portEnTransito) {
			ordenSincro = bscsDao.consultarNumTelefonoContratos(ordenTransito);
			if (siebelDao.confirmaTelefonoEnSiebel(ordenSincro)) {
				ordenesSincro.add(ordenSincro);
			}

		}

		for (OrdenSiebelTO ordenCancelada : portCanceladas) {
			ordenSincro = bscsDao.consultarNumTelefonoContratos(ordenCancelada);
			if (siebelDao.confirmaTelefonoEnSiebel(ordenSincro)) {
				ordenesSincro.add(ordenSincro);
			}
		}

		List<String> valores = null;
		StringBuilder cuerpoCSV = new StringBuilder();
		for (OrdenSincroTO sincro : ordenesSincro) {
			valores = new ArrayList<>();
			valores.add(sincro.getOrdenSiebel().getRowId());
			valores.add(String.valueOf(sincro.getOrdenSiebel().getContractId()));
			valores.add(sincro.getOrdenSiebel().getStatusOrdenRegularizada());
			cuerpoCSV.append(archivoUtil.generarRegistroCSV(valores));
		}
		archivoUtil.generarArchivoCSV(env.getProperty("nombre.csv.sincro.contratos.portabilidades.canceladas"),
				env.getProperty("header.csv.sincro.contratos.portabilidades.canceladas"), cuerpoCSV.toString());
	}

	/** 2    TIME OUT EN FLUJO CREATE CUSTOMER */
	public void regularizarTimeOutFlujoCreateCustomer() {
		/**
		 * TODO
		 * - Invocar SP en BPEL (SELECT)
		 * - Devolver Listado de OMs
		 * - LOOP:
		 * 		- Invocar SP SIEBEL (SELECT) obtener OMs que fallaron en el flujo
		 * 		- Devolver registro de OM
		 * 		- Invocar SP SIEBEL (UPDATE) cancelar linea y cabeceras
		 * - Generar CSV con listado de: row_id | order_num | status actualizado
		 * */
	}

	/** 3	OMS QUE NO HAN LLEGADO A BPEL */
	public void regularizarOmsNoLlegadasABpel() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) ordenes en tránsito correspondientes a Modificaciones y Transferencias
		 * - Devolver Listado de OMs
		 * - LOOP:
		 * 		- Invocar SP BPEL (SELECT) obtener OMs que no llegaron a bpel
		 * 		- Devolver registro de OM
		 * 		- Invocar SP SIEBEL (SELECT)
		 * 		- Devolver registro de ROW_ID
		 * 		- Invocar SP SIEBEL (UPDATE) actualizar estado PIV Open SÓLO cabeceras
		 * - Generar CSV con listado de: row_id | order_num | status actualizado
		 * */
	}

	/** 4.1	OMs  DE Transferencia de PIVCRM */
	public void regularizarOmsDeTransferenciaPIVCRM() {
		
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de Transferencia provenientes de PIV
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 4.2	OMs DE Modificación de PIVCRM */
	public void regularizarOmsDeModificacionPIVCRM() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de modificación provenientes de PIV
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */		
	}

	/** 4.3	OMs DE Transferencia GENERAL */
	public void regularizarOmsDeTransferenciaGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de transferencia
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */		
	}

	/** 4.4	OMs DE MODIFICACIÓN GENERAL */
	public void regularizarOmsDeModificacionGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de modificación
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION, PLAN
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF, PLAN_BSCS
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 4.5	OMs DE Venta GENERAL */
	public void regularizarOmsDeVentaGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de venta
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 4.6	OMs DE Resumir GENERAL */
	public void regularizarOmsDeResumirGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de resumir
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 4.7	OMs DE Suspender GENERAL */
	public void regularizarOmsDeSuspenderGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de suspender
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 4.8	OMs DE Desconección GENERAL */
	public void regularizarOmsDeDesconGeneral() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT) OMs de desconección
		 * - Devolver listado de ROW_ID, SERVICE_NUM, COMPANY_NAME, FECHA_CREACION
		 * - LOOP:
		 * 		- Invocar SP en BSCS (SELECT) enviando el SERVICE_NUM
		 * 		- Devolver registro de PCS, RUT, ESTADO_BSCS, FECHA_MODIF
		 * 		- Comparar registro[i] de SIEBEL vs registro[i] de BSCS
		 * 		- Si Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado COMPLETE para las lineas y cabeceras
		 * 		- Si NO Cumple:
		 * 		- Invocar SP SIEBEL (UPDATE) para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV que cumplen y no cumplen condicion. Campos: row_id | order_num | status actualizado
		 * */	
	}

	/** 5.1	LIMPIEZA DE OMS > Cancelar */
	public void regularizarTransfDesconEstadoCancelar() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT/UPDATE) transferencias de desconección
		 * - Abrir cursor
		 * - LOOP:
		 * 		- Ejecutar UPDATE enviando el ROW_ID para setear estado CANCELLED para las lineas y cabeceras
		 * - Generar salida registros CSV actualizados. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 5.2	LIMPIEZA DE OMS > Completar */
	public void regularizarTransfDesconEstadoCompletar() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT/UPDATE) transferencias de desconección
		 * - Abrir cursor
		 * - LOOP:
		 * 		- Ejecutar UPDATE enviando el ROW_ID para setear estado COMPLETE para las lineas y cabeceras
		 * - Generar salida registros CSV actualizados. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 5.3	LIMPIEZA DE OMS > Error */
	public void regularizarTransfDesconEstadoError() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT/UPDATE) transferencias de desconección
		 * - Abrir cursor
		 * - LOOP:
		 * 		- Ejecutar UPDATE enviando el ROW_ID para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV actualizados. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 5.4	LIMPIEZA DE OMS > PIV Open */
	public void regularizarTransfDesconEstadoPIVOpen() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT/UPDATE) transferencias de desconección
		 * - Abrir cursor
		 * - LOOP:
		 * 		- Ejecutar UPDATE enviando el ROW_ID para setear estado ERROR para las lineas y cabeceras
		 * - Generar salida registros CSV actualizados. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 5.5	OMS sin líneas */
	public void regularizarOmsSinLineas() {
		/**
		 * TODO
		 * - Invocar SP SIEBEL (SELECT/UPDATE) para identificar OMs sin lineas
		 * - Abrir cursor
		 * - LOOP:
		 * 		- Ejecutar UPDATE enviando el ROW_ID para setear estado ERROR para las cabeceras
		 * - Generar salida registros CSV actualizados. Campos: row_id | order_num | status actualizado
		 * */
	}

	/** 5.6	REVISIÓN FINAL */
	public void regularizarRevisionFinal() {
		
		/** PEND: Felipe debe enviar video de cómo ejecuta las query.
		 * 1.- Invocar SP SIEBEL (SELECT)
		 * 2.- Invocar SP SIEBEL (SELECT)
		 * 3.- Si cuadran, ejecutar "4.1 OMs  DE Transferencia de PIVCRM"
		 * */
	}

}
