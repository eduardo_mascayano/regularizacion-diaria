package cl.wom.middleware.automatizacion.actualizacionovedades.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.actualizacionovedades.proceso.ProcesoActualizaNovedades;

/**
 * 
 * Scheduler para realizar carga de novedades de clientes Business.
 *
 * @author Eduardo Mascayano - Tinet
 */

@Component
public class SchedulerActualizacionNovedades {

    private final Logger log = Logger.getLogger(SchedulerActualizacionNovedades.class);

    @Autowired
    ProcesoActualizaNovedades proceso;

    /**
     * Ejecuta el proceso de carga de novedades de Lunes a Viernes (Configurable).
     * 
     */
    //@Scheduled(cron = "${horario.ejecucion.scheduler}", zone="America/Santiago")
    @Scheduled(fixedDelayString = "3000")
    public void iniciarProcesoActualizaNovedades() {

        log.info("[iniciarProcesoActualizaNovedades] Inicio");
        proceso.init();
        log.info("[iniciarProcesoActualizaNovedades] Fin");

    }
}
