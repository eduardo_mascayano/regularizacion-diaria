package cl.wom.middleware.automatizacion.regularizacionordenes.to;

import java.io.Serializable;
import java.util.Date;

public class OrdenSiebelTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String rowId;
	private String orderNum;
	private String srvRowId;
	private Date fechaCreacion;
	private String companyName;
	private Long itemServiceNum;
	private Long contractId;
	private String statusOrdenRegularizada;

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getSrvRowId() {
		return srvRowId;
	}

	public void setSrvRowId(String srvRowId) {
		this.srvRowId = srvRowId;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getItemServiceNum() {
		return itemServiceNum;
	}

	public void setItemServiceNum(Long itemServiceNum) {
		this.itemServiceNum = itemServiceNum;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getStatusOrdenRegularizada() {
		return statusOrdenRegularizada;
	}

	public void setStatusOrdenRegularizada(String statusOrdenRegularizada) {
		this.statusOrdenRegularizada = statusOrdenRegularizada;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrdenSiebelTO [rowId=");
		builder.append(rowId);
		builder.append(", orderNum=");
		builder.append(orderNum);
		builder.append(", srvRowId=");
		builder.append(srvRowId);
		builder.append(", fechaCreacion=");
		builder.append(fechaCreacion);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", itemServiceNum=");
		builder.append(itemServiceNum);
		builder.append(", contractId=");
		builder.append(contractId);
		builder.append(", statusOrdenRegularizada=");
		builder.append(statusOrdenRegularizada);
		builder.append("]");
		return builder.toString();
	}

}
