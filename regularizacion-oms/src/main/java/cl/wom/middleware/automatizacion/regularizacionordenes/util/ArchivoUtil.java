package cl.wom.middleware.automatizacion.regularizacionordenes.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import cl.wom.middleware.automatizacion.regularizacionordenes.exception.AplicacionException;

/**
 * Utilitario para manejo de archivos.
 *
 * @author Eduardo Mascayano - Tinet
 */
@Component
@PropertySource("file:${APP_ENV}")
public class ArchivoUtil {

	private final Logger log = Logger.getLogger(ArchivoUtil.class);

	@Autowired
	private Environment env;

	private static final String NEW_LINE = "\n";

	public String generarRegistroCSV(List<String> valores) {

		StringBuilder sb = new StringBuilder();

		for (String valor : valores) {
			sb.append(valor).append(',');
		}
		sb.deleteCharAt(sb.length());
		sb.append(NEW_LINE);
		return sb.toString();
	}

	public void generarArchivoCSV(String nombreArhivoCSV, String cabeceraCSV, String cuerpoCSV) {

		try (PrintWriter pw = new PrintWriter(new File(env.getProperty("csv.path") + nombreArhivoCSV))) {

			StringBuilder sb = new StringBuilder();
			sb.append(cabeceraCSV).append(NEW_LINE).append(cuerpoCSV);
			pw.write(sb.toString());
		} catch (FileNotFoundException e) {
			throw new AplicacionException("Error al generar el archivo CSV " + nombreArhivoCSV);
		}
	}
}
