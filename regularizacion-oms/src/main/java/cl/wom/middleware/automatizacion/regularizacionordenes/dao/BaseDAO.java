package cl.wom.middleware.automatizacion.regularizacionordenes.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 * Clase utilitaria para conexion a base de datos.
 *
 * @author Eduardo Mascayano - Tinet
 */
public class BaseDAO {

    private static Logger log = Logger.getLogger(BaseDAO.class);

    /**
     * Url.
     */
    protected String url;

    /**
     * User.
     */
    protected String user;

    /**
     * Password.
     */
    protected String password;

    public Connection getConnection() {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            log.error("[getConnection] Error al obtener conexion", e);
        }
        return connection;

    }

    public void closePSConnection(ResultSet rs, PreparedStatement ps, Connection connection) {

        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("[closePSConnection] Error al cerrar resultSet", e);
        }
        try {
            if (ps != null && !ps.isClosed()) {
                ps.close();
            }
        } catch (SQLException e) {
            log.error("[closePSConnection] Error al cerrar preparedStatement", e);
        }
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("[closePSConnection] Error al cerrar conexion", e);
        }
    }

    public void closeCSConnection(ResultSet rs, CallableStatement cs, Connection connection) {

        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
        } catch (SQLException e) {
            log.error("[closeCSConnection] Error al cerrar resultSet", e);
        }
        try {
            if (cs != null && !cs.isClosed()) {
                cs.close();
            }
        } catch (SQLException e) {
            log.error("[closeCSConnection] Error al cerrar preparedStatement", e);
        }
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("[closeCSConnection] Error al cerrar conexion", e);
        }
    }

    public void closeCSConnection(CallableStatement cs, Connection connection) {

        try {
            if (cs != null && !cs.isClosed()) {
                cs.close();
            }
        } catch (SQLException e) {
            log.error("[closeCSConnection] Error al cerrar preparedStatement", e);
        }
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("[closeCSConnection] Error al cerrar conexion", e);
        }
    }

    /**
     * GetBigDecimal.
     * 
     * @param rs rs.
     * @param key key.
     * @return string.
     */
    public BigDecimal getBigDecimal(ResultSet rs, String key) {

        try {
            if (rs.getObject(key) != null && !(String.valueOf(rs.getObject(key)).isEmpty())) {
                return new BigDecimal(String.valueOf(rs.getObject(key)));
            }
        } catch (Exception e) {
            log.error("[getBigDecimal] Error al obtener el dato" + e.getMessage());
        }
        return null;
    }

    /**
     * GetLong.
     * 
     * @param rs rs.
     * @param key key.
     * @return long.
     */
    public long getLong(ResultSet rs, String key) {

        try {
            if (rs.getObject(key) != null) {
                return rs.getLong(key);
            }
        } catch (Exception e) {
            log.error("[GetLong] Error al obtener el dato" + e.getMessage());
        }
        return 0;
    }

    /**
     * getBoolean.
     * 
     * @param rs rs.
     * @param key key.
     * @return Boolean.
     */
    public Boolean getBoolean(ResultSet rs, String key) {

        Boolean valor = null;
        try {
            if (rs.getObject(key) != null) {
                valor = Boolean.valueOf(String.valueOf(rs.getObject(key)));
            }
        } catch (Exception e) {
            log.error("[getBoolean] Error al obtener el dato" + e.getMessage());
        }
        return valor;
    }

    /**
     * GetString.
     * 
     * @param rs rs.
     * @param key key.
     * @return string.
     */
    public String getString(ResultSet rs, String key) {

        try {
            if (rs.getObject(key) != null) {
                return String.valueOf(rs.getObject(key));
            }
        } catch (Exception e) {
            log.error("[GetString] Error al obtener el dato" + e.getMessage());
        }
        return null;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getUser() {

        return user;
    }

    public void setUser(String user) {

        this.user = user;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

}
