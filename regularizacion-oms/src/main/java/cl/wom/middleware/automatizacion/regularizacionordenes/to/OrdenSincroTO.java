package cl.wom.middleware.automatizacion.regularizacionordenes.to;

import java.io.Serializable;

public class OrdenSincroTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private OrdenSiebelTO ordenSiebel;
	private OrdenBscsTO ordenBscs;

	public OrdenSiebelTO getOrdenSiebel() {
		return ordenSiebel;
	}

	public void setOrdenSiebel(OrdenSiebelTO ordenSiebel) {
		this.ordenSiebel = ordenSiebel;
	}

	public OrdenBscsTO getOrdenBscs() {
		return ordenBscs;
	}

	public void setOrdenBscs(OrdenBscsTO ordenBscs) {
		this.ordenBscs = ordenBscs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrdenSincroTO [ordenSiebel=");
		builder.append(ordenSiebel);
		builder.append(", ordenBscs=");
		builder.append(ordenBscs);
		builder.append("]");
		return builder.toString();
	}

}
