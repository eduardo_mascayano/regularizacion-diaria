package cl.wom.middleware.automatizacion.regularizacionordenes.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("file:${APP_ENV}")
public class EmailUtil {

    public String generarEmailExito() {

        StringBuilder html = new StringBuilder();
        html.append("<html><body>");
        html.append("Estimado Usuario:<br/>");
        html.append("El proceso de Actualización de Novedades correspondiente al día "
                + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " fue ejecutado con ÉXITO.<br/>");
        html.append("</body><html>");

        return html.toString();
    }

    public String generarEmailError(String mensajeError) {

        StringBuilder html = new StringBuilder();
        html.append("<html><body>");
        html.append("Estimado Usuario:<br/>");
        html.append("El proceso de Actualización de Novedades correspondiente al día "
                + new SimpleDateFormat("dd/MM/yyyy").format(new Date()) + " fue ejecutado con ERRORES:<br/><br/>");
        html.append("<div style='color:red;font-weight: bold;'>" + mensajeError + "</div>");
        html.append("<br/><br/>");
        html.append(
                "Favor revisar log del proceso para mayor detalle o ejecutar procedimiento de actualización de forma manual");
        html.append("</body><html>");

        return html.toString();
    }
}
